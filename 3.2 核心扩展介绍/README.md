##3.2 核心扩展介绍 @dogstar
  
###一、π框架中的扩展类库
####1)扩展类库的简单介绍
 + 即插即用 
 + 可重用的、业务无关的基础设施类库  
 + 致力于与开源项目一起提供企业级的解决方案！
  
####2)目前有哪些扩展类库 
 + 23+个
  
###二、如何使用扩展类库
####1)使用步骤
 + 1、下载安装
 + 2、配置
 + 3、注册
 + 4、使用
  
####2)扩展类库使用示例
 + Log4php日志
 + View视图渲染
  
###三、如何开发、贡献扩展类库
 + 如何开发尚未提供的扩展类库
 + 如何分享
