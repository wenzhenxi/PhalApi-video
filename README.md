#建议改用
```
这份md：
http://git.oschina.net/dogstar/PhalApi/wikis/[8.1]-PhalApi%E8%A7%86%E9%A2%91%E6%95%99%E7%A8%8B

对应官网访问效果：
http://www.phalapi.net/wikis/%5b8.1%5d-PhalApi%e8%a7%86%e9%a2%91%e6%95%99%e7%a8%8b.html
```
#phalapi教程视频大纲#

![](http://webtools.qiniudn.com/master-LOGO-20150410_50.jpg)

##前言##
***先在这里感谢各录制小组的同学，是你们让世界看到PhalApi的视频教程！***

此为phalapi教程视频大纲,分为基础教程,进阶教程,实战教程三类

附上:

喵了个咪的博客:[w-blog.cn](w-blog.cn)

官网地址:[http://www.phalapi.net/](http://www.phalapi.net/ "PhalApi官网")

开源中国Git地址:[http://git.oschina.net/dogstar/PhalApi/tree/release](http://git.oschina.net/dogstar/PhalApi/tree/release "开源中国Git地址")

##1 基础教程##

###1.1 环境搭建,安装和helloword###

phalapi介绍以及使用场景

建议使用liunx和各项环境配置最低

推荐IDE

框架编写的install展示

运行helloword!

###1.2 初识PhalApi###

目录功能讲解

数据库建库建表

连接数据库官方DEMO

###1.3 init介绍以及配置文件讲解log记录###

init文件作用内容讲解

配置文件使用讲解

以及log记录的活灵活用

###1.4 请求返回###

请求的参数验证getRules方法使用讲解

返回json数据讲解

返回报错讲解

###1.5 国际化和自动生成文档###

返回国际化T方法讲解

自动生成文档规范讲解

如何查看

注意不要使用缓存会出不来

###1.6 Model操作讲解###

Model表配置

NotORM的使用

简单的 curd 执行原生sql

###1.7 简单的小项目###

对用户的一个登录注册等操作的接口实际项目编写实战

并且总结

##2 进阶教程##

###2.1 DI思想讲解###

DI的作用返回

已经DI的实现

###2.2 API,DOMAIN,MODEL灵活使用###

为什么phalapi要使用三层结构

三层结构的优势 

###2.3 自动加载和拦截器###

自动加载机制的活灵活用

定义自己的项目功能文件

使用拦截器过滤参数并且使用token验证请求

###2.4 自定义参数验证规则###

对getRules自定义参数验证规则

一个邮箱验证的小案例

###2.5 NotORM进阶细节使用###

方法补全

事务操作

###2.6 数据库读写分离以及多库使用###

数据库实现读写分离

实现跨库使用

###2.7 使用缓存###

使用缓存来实现缓存API请求结果

使用缓存来实现对数据库查询结果进行缓存

##3 实战教程##

###3.1 多项目多版本处理###

###3.2 核心拓展介绍###

**官网QQ交流群:421032344  欢迎大家的加入!**