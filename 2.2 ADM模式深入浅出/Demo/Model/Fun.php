<?php

class Model_Fun {

    public function publish($msg) {
        $contents = DI()->cache->get('fun_contents');

        if ($contents !== NULL) {
            $contents .= "|$msg";
        } else {
            $contents = $msg;
        }

        DI()->cache->set('fun_contents', $contents, 86400);
    }

    public function read() {
        $contents = DI()->cache->get('fun_contents');
        return $contents !== NULL ? explode('|', $contents) : array();
    }

    public function clear() {
        DI()->cache->delete('fun_contents');
    }
}
