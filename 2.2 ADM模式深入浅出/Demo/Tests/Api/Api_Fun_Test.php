<?php
/**
 * PhpUnderControl_ApiFun_Test
 *
 * 针对 ../../Api/Fun.php Api_Fun 类的PHPUnit单元测试
 *
 * @author: dogstar 20160416
 */

require_once dirname(__FILE__) . '/../test_env.php';

if (!class_exists('Api_Fun')) {
    require dirname(__FILE__) . '/../../Api/Fun.php';
}

class PhpUnderControl_ApiFun_Test extends PHPUnit_Framework_TestCase
{
    public $apiFun;

    protected function setUp()
    {
        parent::setUp();

        $this->apiFun = new Api_Fun();
    }

    protected function tearDown()
    {
    }


    /**
     * @group testGetRules
     */ 
    public function testGetRules()
    {
        $rs = $this->apiFun->getRules();
        $this->assertNotEmpty($rs);
    }

    /**
     * @group testPublish
     */ 
    public function testPublish()
    {
        //Step 1. 构建请求URL
        $url = 'service=Fun.Publish';
        $params = array('msg' => 'test ADM pattern');

        //Step 2. 执行请求	
        $rs = PhalApi_Helper_TestRunner::go($url, $params);

        //Step 3. 验证
        $this->assertNotEmpty($rs);
        $this->assertArrayHasKey('code', $rs);
        $this->assertEquals(1, $rs['code']);
    }

    /**
     * @group testRead
     */ 
    public function testRead()
    {
        //Step 1. 构建请求URL
        $url = 'service=Fun.Read';

        //Step 2. 执行请求	
        $rs = PhalApi_Helper_TestRunner::go($url);

        //Step 3. 验证
        $this->assertNotEmpty($rs);
        $this->assertArrayHasKey('contents', $rs);
        $this->assertTrue(is_array($rs['contents']));
    }

}
