<?php

class Domain_Fun {

    public function publish($msg) {
        $model = new Model_Fun();
        $model->publish($msg);
    }

    public function read() {
        $model = new Model_Fun();

        $contents = $model->read();
        $model->clear();

        return $contents;
    }
}
