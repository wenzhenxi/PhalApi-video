<?php

class Api_Fun extends PhalApi_Api {

    public function getRules() {
        return array(
            'publish' => array(
                'msg' => array('name' => 'msg', 'min' => 1, 'require' => true),
            ),
        );
    }

    public function publish() {
        $rs = array('code' => 1);

        $domain = new Domain_Fun();
        $domain->publish($this->msg);

        return $rs;
    }

    public function read() {
        $rs = array('contents' => array());

        $domain = new Domain_Fun();
        $rs['contents'] = $domain->read();

        return $rs;
    }
}
