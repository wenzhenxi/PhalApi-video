<?php
/**
 * 翻译说明：
 * 1、带大括号的保留原来写法，如：{name}，会被系统动态替换
 * 2、没在以下出现的，可以自行追加
 * @author dogstar <chanzonghuang@gmail.com> 2015-02-09
 */

return array(
    'Hi {name}, welcome to use PhalApi!' => '{name}您好，欢迎使用PhalApi！',
    'user not exists'                    => '用户不存在',
    "test error"                         => "测试请求错误",
    "Email {name} {data} {phone} {url}"  => "{name}你好,你收到一封邮件类容是{data},你的号码是{phone},点击下方URL领奖{url}",
);
