<?php

class Model_User extends PhalApi_Model_NotORM {

    protected function getTableName($id) {

        return 'user';
    }

    /**
     * 用户注册
     */
    public function useradd($data) {

        return $this->getORM()->insert(array(
            "username" => $data->username,
            "password" => $data->password,
            "phone"    => $data->phone,
            "time"     => time()
        ));
    }

    /**
     * 通过用户名称获取信息
     */
    public function getInfoByUserName($username) {

        return $this->getORM()->where("username", $username)->fetch();
    }

    /**
     * 用户登录接口
     */
    public function Userlogin($data) {

        return $this->getORM()->select("uid")->where("username", $data->username)->where("password", $data->password)->fetch();
    }

    /**
     * 通过用户ID获取详情
     */
    public function getInfoByuId($uId) {

        return $this->getORM()->where("uId", $uId)->fetch();
    }

    /**
     * 获取用户列表
     */
    public function getUserList() {

        return $this->getORM()->order("time desc")->fetchAll();
    }
}
