<?php

/**
 * 默认接口服务类
 * @author: dogstar <chanzonghuang@gmail.com> 2014-10-04
 */
class Api_Config extends PhalApi_Api {

    public function getRules() {

        return array(
            'index'    => DI()->config->get("app.Config-index"),
            'request'  => array(
                "id"   => array("name" => "uId", "type" => "int", "require" => true, "min" => 1, "max" => "99999"),
                "name" => array(
                    "name"    => "username",
                    "type"    => "string",
                    "default" => "QQ用户",
                    "min"     => 6,
                    "format"  => "utf8"
                )
            ),
            'response' => array()
        );
    }

    /**
     * 这是请求接口
     */
    public function request() {

        throw new PhalApi_Exception_BadRequest(T("Email {name} {data} {phone} {url}", array(
                "name"  => "喵咪",
                "data"  => "您获得了天猫优惠券",
                "phone" => "132456",
                "url"   => "www.baidu.com"
            )), 1);
        return array(
            "id"   => $this->id,
            "name" => $this->name
        );
    }

    /**
     * 这是返回接口
     */
    public function response() {

        return "这是返回接口";
    }

    /**
     * 默认接口服务
     * @desc 这个是测试不可以使用的
     *
     * @return string name 名称
     * @return string title 标题
     * @return string content 内容
     * @return string version 版本，格式：X.X.X
     * @return int time 当前时间戳
     */
    public function index() {

        DI()->logger->log("info", "200", DI()->config->get("app.Config-index.userId.type"));
        DI()->logger->debug("400", DI()->config->get("app.Config-index.userId.type"));
        return DI()->config->get("app.Config-index.userId.type");
    }
}
